k8sconfgen::simple_ingress { 'sidenav-ui':
  host              => lookup('hl::hltech::platform_host'),
  path              => '/app/side-nav',
  service_name      => sidenav-ui,
  service_port      => 'web',
  path_prefix_strip => true,
  filename          => 'ingress.yaml',
}
