# SideNav UI

## Table of Contents

1. [**Overview**](#Overview)
2. [**Links**](#Links)
3. [**Authentication & authorization**](#Auth)
3. [**Execution**](#Execution)
5. [**Development**](#Development)

## Overview <a name="Overview"></a>

**TODO**: provide short description about YOUR application

## Links <a name="Links"></a>

- Application on environments: [SIT](https://platform.sit.k8s.hltech.dev.hl.directory/app/side-nav/), [DEMO](https://platform.demo.tech.hl.uk/app/side-nav/), [QAM](https://platform.qam.k8s.hltech.dev.hl.directory/app/side-nav/)
- Jenkins: ...
- JIRA: ...
- Bitbucket: ...

**TODO**: provide proper links to application tools.

## Authentication & authorization <a name="Auth"></a>

Users are authenticated using standard Windows credentials via [Platform Auth Server](https://bitbucket-colab.hargreaveslansdown.co.uk/projects/PLAT/repos/authserver/browse)

**TODO**: Provide information authorization groups and roles inside the application.

## Execution

### Application artifact

The application is published as Docker image containing:

- Apache as a http server (using Apache base container)
- static files like html, js, css, media files.

### Configuration

When running on environment the application is configured using environment variables passed to Docker container.

Local development configuration can be set using public/config.js file

| Property | Description |
|:------------- |:-------------|
| APP_BASE_PATH | Path under which application is accessible |
| API_BASE_PATH | Base URL for API endpoint |
| AUTH_CLIENT_ID | OAuth client identifier |
| AUTH_AUTH_ENDPOINT |OAuth server authorisation endpoint |
| AUTH_TOKEN_ENDPOINT |OAuth server token endpoint|
| AUTH_LOGOUT_ENDPOINT |OAuth server logout endpoint |

### Monitoring 

The Apache [base container](https://confluence.hargreaveslansdown.co.uk/display/CON/Apache) provides a HTTP monitoring endpoint on port 9999.  
This will provide a 200 response to requests for /health and so can function as the health check and readiness endpoint for Kubernetes.


## Development <a name="Development"></a>

See [Development Guide](DEVELOPMENT.md)