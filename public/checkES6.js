try {
    // Check if ES6 features are enabled
    Promise.resolve();
} catch (n) {
    var e = document.createElement('h3');
    e.style.margin = '40px auto';
    e.style.maxWidth = '800px';
    e.style.textAlign = 'center';
    e.style.fontFamily = 'Arial, Helvetica, sans-serif';
    e.innerText =
        'Application will not work correctly in this browser.\n' +
        '\n' +
        'Please use newer, more secure internet browser such as: Google Chrome, Mozilla Firefox or Microsoft Edge to access this application.\n' +
        '\n' +
        'If you do not have these on your computer, please contact Service Desk and request update/installation.';
    document.body.appendChild(e);
    var rootElement = document.querySelector('#root');
    rootElement.style.setProperty('display', 'none', 'important');
}
