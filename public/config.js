window.__config__ = {

    // Path under which application is accessible (/ on localhost)
    APP_BASE_PATH: '/',

    // Base path for API service
    API_BASE_URL: '/api',

    // Base path for platform service
    AUTH_SERVER_URL: 'https://platform.sit.k8s.hltech.dev.hl.directory/api/platform-wam-service',
};
