#!/bin/bash
set -e

cd /var/www/html/

# Sets href attr of <base> element in index.html to APP_BASE_PATH (defaulting to "/")
sed -i 's:<base href="/":<base href="'${APP_BASE_PATH:-/}'":gI' ./index.html

# Substitutes env variables in config.js
echo "$(envsubst < config.js)" > config.js

# Substitutes env variables in platform.manifest.json
echo "$(envsubst < platform.manifest.json)" > platform.manifest.json
