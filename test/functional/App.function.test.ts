import {Mockiavelli} from "mockiavelli";
import {mockPlatformAuth} from "@hltech/puppeteer-utils";

const APP_URL = `http://localhost:9000`;

describe('Application', () => {

    let mockiavelli: Mockiavelli;

    beforeEach(async () => {
        mockiavelli = await Mockiavelli.setup(page);
        mockPlatformAuth(mockiavelli, {
            authServerUrl: `${APP_URL}/api/platform-wam-service`,
            user: {
                user_name: 'test-user',
                authorities: ['test-role'],
            },
        });
    });

    test('load and render application name', async () => {
        await page.goto(APP_URL);
        await expect(page).toMatch('SideNav');
    });

});
