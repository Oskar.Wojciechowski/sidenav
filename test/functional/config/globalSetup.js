const path = require('path');
const {setup: setupPuppeteer} = require('jest-environment-puppeteer');
const {setupServer} = require('@hltech/puppeteer-utils');

module.exports = async function globalSetup(globalConfig) {
    await setupPuppeteer(globalConfig);
    await setupServer({
        directory: path.resolve(__dirname, '../../../build'),
        port: 9000
    });
};