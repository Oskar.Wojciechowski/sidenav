import React from 'react';
import ReactDOM from 'react-dom';
import { conf } from './api/config/app-config';

import '@hltech/ui-elements/dist/webfonts.css';
import { PlatformAuth } from '@hltech/platform-auth';
import { setGlobalStyles } from '@hltech/ui-elements';
import {App} from "./App";


setGlobalStyles();

ReactDOM.render(
    <PlatformAuth config={{
        authServerUrl: conf('AUTH_SERVER_URL')
    }}>
        <App />
    </PlatformAuth>,
    document.getElementById('root'),
);
