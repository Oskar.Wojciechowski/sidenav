import { Colors, css, Header } from '@hltech/ui-elements';
import React from "react";

interface HelloProps {
    text: string;
}

export const Hello: React.FC<HelloProps> = ({ text }) => {
    return (
        <>
            <Header level={5} align={"center"}>{text}</Header>
            <div className={container}>
                <img className={img} src="./cow.jpg" alt="cow" />
            </div>
        </>
    )
}

const img = css({
    maxWidth: 500,
    border: `solid 10px ${Colors.cliftonNavy}`,
});

const container = css({
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
});