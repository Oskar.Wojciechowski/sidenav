import React, {useEffect, useRef, useState} from 'react';
import {bottomSpacer, header, headerLink, leadership, operations, providers, selected, sticky} from "./App.styles";
import {getDimensions} from "./helpers";
import {cx} from "@hltech/ui-elements";

export const App: React.FC = () => {
    const [visibleSection, setVisibleSection] = useState('');

    const leadershipRef = useRef(null);
    const providerRef = useRef(null);
    const operationsRef = useRef(null);

    const sectionRefs = [
        {section: "Leadership", ref: leadershipRef},
        {section: "Providers", ref: providerRef},
        {section: "Operations", ref: operationsRef},
    ];

    useEffect(() => {
        const handleScroll = () => {
            const scrollPosition = window.scrollY;

            const selected = sectionRefs.find(({section, ref}) => {
                const ele = ref.current;
                if (ele) {
                    const {offsetBottom, offsetTop} = getDimensions(ele);
                    return scrollPosition > offsetTop && scrollPosition < offsetBottom;
                }
            });

            if (selected && selected.section !== visibleSection) {
                setVisibleSection(selected.section);
            }
        };

        handleScroll();
        window.addEventListener("scroll", handleScroll);
        return () => {
            window.removeEventListener("scroll", handleScroll);
        };
    }, [visibleSection]);

    console.log('visibleSection', visibleSection)

    return (
        <>
            <div className={sticky}>
                <div className={header}>
                    <button type="button"
                            className={cx(headerLink, visibleSection === 'Leadership' ? selected : undefined)}>
                        Leadership
                    </button>
                    <button type="button"
                            className={cx(headerLink, visibleSection === 'Providers' ? selected : undefined)}>
                        Providers
                    </button>
                    <button type="button"
                            className={cx(headerLink, visibleSection === 'Operations' ? selected : undefined)}>
                        Operations
                    </button>
                </div>
            </div>
            <div className={leadership} id="Leadership" ref={leadershipRef}/>
            <div className={providers} id="Providers" ref={providerRef}/>
            <div className={operations} id="Operations" ref={operationsRef}/>
            <div className={bottomSpacer}/>
        </>
    );
};