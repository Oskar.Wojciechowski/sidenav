import { PLATFORM_AUTH_HEADERS } from '@hltech/platform-auth';
import Axios from 'axios';
import {conf} from '../config/app-config';

export const axios = Axios.create({
    baseURL: conf('API_BASE_URL'),
    headers: { ...PLATFORM_AUTH_HEADERS },
});

