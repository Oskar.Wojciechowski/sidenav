let configStore: Record<string, string> = {};

export function setConfig(configObject: Record<string, string>) {
    configStore = configObject;
}

/**
 * Populate configuration from window.__config__ object, if executed in browser context
 */
if (typeof window !== 'undefined') {
    // @ts-ignore
    setConfig(window['__config__'] || {});
}

/**
 * Return given config key, throwing error if not defined
 */
export function conf(key: string): string {
    if (!configStore) {
        throw new Error(`Configuration in not defined. Probably it was not initialized`);
    }
    return configStore[key];
}

/**
 * Get router BrowserRouter basename from <base href=..> attribute without trailing
 */
export const ROUTER_BASE_NAME = (function getBaseName() {
    const base = document ? document.querySelector('base') : undefined;
    const baseHref = base ? base.getAttribute('href') || '' : '';
    return baseHref.replace(/\/$/, '');
})();
