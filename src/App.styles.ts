import {Colors, css, cx} from "@hltech/ui-elements";

export const topSpacer = css({
    height: '50vh'
})

export const bottomSpacer = css({
    height: '110vh'
})

export const header = css({
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: Colors.anchorBlueAccent,
})

export const headerLink = css({
    padding: '20px',
    color: Colors.britishSky,
    fontWeight: 'bold',
    border: 'none',
    borderBottom: '3px solid transparent',
    cursor: 'pointer',
    outline: 'none',
})

export const selected = css({
    borderBottom: `3px solid ${Colors.britishSkyDarker}`,
    color: Colors.cliftonNavy,
})

export const section = css({
    height: '40vh',
})

export const sticky = css({
    position: 'sticky',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 10,
})

export const leadership = cx(
    section,
    css({backgroundColor: Colors.britishSkyDarkest})
)

export const providers = cx(
    section,
    css({backgroundColor: Colors.cabotRed})
)

export const operations = cx(
    section,
    css({backgroundColor: Colors.kendalGold})
)