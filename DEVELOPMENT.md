# SideNav Development guide

This app is developed as single-page web application (SPA), bootstrapped using [HL Tech Template](https://bitbucket-colab.hargreaveslansdown.co.uk/projects/FE/repos/cra-template-hltech/browse) for [Create React App](https://create-react-app.dev/).

**Table of Contents:**

1. [Prerequisites](#prerequisites)
2. [Installation](#installation)
3. [Directory structure](#directory-structure)
4. [Running locally](#running-locally)
5. [Testing](#testing)
6. [Building](#building)
7. [Code style](#code-style)
8. [Application config](#config)
9. [Continuous Integration](#ci)
10. [Containerization](#containerization)
11. [Deployment](#deployment)

## <a name="prerequisites"></a> Prerequisites

You need to have Git, Node (> 10) and npm (>= v7) installed and configured properly. Please follow this [guide](https://confluence.hargreaveslansdown.co.uk/display/DG/Development+environment+setup).

## <a name="installation"></a> Installation

Clone this git repository, enter the project directory, then run:

```sh
npm install
```

In case of problems make sure your [development environment is configured correctly](https://confluence.hargreaveslansdown.co.uk/display/DG/Development+environment+setup).

## <a name="directory-structure"></a> Directory structure

-   `docker/` - docker image definition. See [Containerization](#containerization).
-   `kubernetes/` - kubernetes deployment configuration. See [Deployment](#deployment).
-   `public/` - index.html and other static assets not included in application bundle. See [Using the Public Folder](https://create-react-app.dev/docs/using-the-public-folder/#when-to-use-the-public-folder)
-   `src/` - application source files
-   `test/` - tests, with separate directories for unit, integration and functional tests. See [Testing](#testing)

There are also serveral generated directories, not checked out into git:
-   `build/` - application bundle - will be packed inside Docker container (created by running `npm run build`)
    -   `build/static/` - bundled JS, CSS (and other) assets. Each file in this directory must have a unique, content-dependend hash appended to the filename to allow correct caching.
-   `node_modules/` - application dependencies
-   `reports/` - various test reports, generated during [testing](#testing)

## <a name="running-locally"></a> Running locally

To start the application locally with dev server:

```sh
npm start
```

Application will be served at http://localhost:3000

By default application uses APIs from SIT environment. To change it edit the `proxy` property in `package.json` file

## <a name="testing"></a> Testing

To run all tests

```sh
npm test
```

or just simply

```sh
npm t
```

To run single suite of tests

```shell script
npm test test/unit
npm test test/integration
npm test test/functional
```

To run tests with coverage

```shell script
npm test --coverage
```

To run tests directly in your IDE see [Integrate Jest with Webstorm/IntelliJ](https://confluence.hargreaveslansdown.co.uk/pages/viewpage.action?pageId=659293579)

Refer to `jest.config.js` file for detailed configuration

## <a name="building"></a> Building

To build the application run:

```sh
npm run build
```

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />

## <a name="code-style"></a> Code style

[Prettier](https://prettier.io/docs/en/install.html) is used for automatic and opinionated code formatting.

Together with [pretty-quick](https://github.com/azz/pretty-quick) and [husky](https://github.com/typicode/husky) it **automatically reformats code before commit**.

You can also format files directly in IDE by installing configuring [Editor Integrations](https://prettier.io/docs/en/editors.html)

All relevant configuration is stored in following files:

-   [`prettier.config.js`](./prettier.config.js)
-   [`husky.config.js`](./husky.config.js)

## <a name="config"></a> Application config

All configuration values that vary between deployments/environment are separated from app source file and bundle, and are located in:

-   [`public/config.js`](./public/config.js) - for local development
-   [`docker/config.js`](./docker/config.js) - for deployment environments.
    -   Placeholders in this file are substituted with environment variables when docker container starts (see [`initialize.sh`]('./docker/initialise.sh))
    -   Env variables are injected to Docker container by Kubernetes/Kustomize. See [`kubernetes/base/envs.properties`](./kubernetes/base/envs.properties) for example.
    -   Consult [Application - deployment config](https://confluence.hargreaveslansdown.co.uk/display/DG/Application+-+deployment+config+-+Kustomize+-+basics) guide for more information on env configuration.

`config.js` is loaded via `<link>` tag in [`index.html`](./public/index.html) before main application bundle. The script assigns an object literal to `window.__config__` global object. This object is then read by application in runtime - see [`src/api/config/app-config.ts`](./src/api/config/app-config.ts)

Important rules:

-   configuration keys must be UPPERCASE_SNAKE_CASE
-   [`public/config.js`](./public/config.js) and [`docker/config.js`](./docker/config.js) must contain exactly the same config keys
-   the value of every key in [`docker/config.js`](./docker/config.js) must be an environment variable string named after the key: `PARAM: '$PARAM'`


### <a name="base-path"></a> Base path config

The application needs to be able to run on different base paths - `/` in dev and usually `/app/applicaion-name` on environment.

This is done by setting `<base href>` and ensuring that all URL to assets inside the application are relative:

-   [`public/index.html`](./public/index.html) includes `<base href="/">` tag at the top of `<meta>`
-   `BASE_PATH` env var is set when starting container
-   Relative paths for all compiled assets:
    -   Create-React-App: `"homepage": "./"` in `package.json` (see [docs](https://facebook.github.io/create-react-app/docs/deployment#serving-the-same-build-from-different-paths))
    -   Webpack: `output.publicPath: "./"` (see [docs](https://webpack.js.org/configuration/output/#outputpublicpath))

## <a name="ci"></a> Continuous Integration

This project uses [Jenkinsfile](https://jenkins.io/doc/book/pipeline/jenkinsfile/) to define project pipeline configuration in a declarative manner.

Additionally, a [Jenkins Shared Library](https://bitbucket-colab.hargreaveslansdown.co.uk/projects/HLTDEVOPS/repos/jenkins-shared-library/browse) was developed at HL Tech to uniform pipelines across all projects.

Pipeline configuration is stored at [`Jenkinsfile`](./Jenkinsfile)

See [uiPipeline docs](https://bitbucket-colab.hargreaveslansdown.co.uk/projects/HLTDEVOPS/repos/jenkins-shared-library/browse/docs/pipelines/uiPipeline.md) for all available options.

## <a name="containerization"></a> Containerization

Docker is used for containerization of this application. A new Docker image is automatically created and published to Docker registry for every commit on master branch. The image contains application bundle and a configured web server.
 
Following "Build Once, Deploy Anywhere" principle, the same image is later deployed on different environments.

Most important config files:

-   [`docker/Dockerfile`](./docker/Dockerfile) - based on HL Apache base image. It copies all necessary files into the container.
-   [`docker/local.conf`](./docker/local.conf) - Apache configuration file (on the top of baseline HL configuration).
-   [`docker/initialise.sh`](./docker/initialise.sh) - Setup script used to set `<BASE href />` tag in index.html based on BASE_PATH env variable and substitues config values in `config.js`

Building image locally and running it at [`http://localhost:8080`](http://localhost:8080)

```bash
npm run build
docker build -f docker\Dockerfile .
docker run -p 8080:8080 -e BASE_PATH=/ ENV_PARAM=some_value <container-id>
```

Useful links:
-   [Containerisation - Confluence](https://confluence-colab.hargreaveslansdown.co.uk/display/CON/Containerisation+Home)


## <a name="deployment"></a> Deployment

We use [Kubernetes](https://kubernetes.io/) with [Kustomize](https://kustomize.io/) for automatic deployment and configuration management.

Consult [Service deployment configuration - Kustomize](https://confluence-colab.hargreaveslansdown.co.uk/display/DG/Service+deployment+configuration+-+Kustomize) for detailed information.

Important notes

-   [`kubernetes/base/envs.properties`](./kubernetes/base/envs.properties) should contain all configuration properties. They are used to replace environment variable strings in [`docker/config.js`](./docker/config.js) when container is started
    -   any config values that **vary** between deployment should be set to `PLACEHOLDER` in [`base/envs.properties`](./kubernetes/base/envs.properties) and set to real value in `{env}/envs.properties`, together with appropriate `configMapGenerator` in overlays
    -   if no config values vary between deployments then you only need [`base/envs.properties`](./kubernetes/base/envs.properties) file
-   `resources`, `readinessProbe` `livenessProbe` in [`kubernetes/base/conventional-app.yaml`](./kubernetes/base/conventional-app.yaml) are tuned to the needs of UI applications
-   `/health` endpoint on port `9999` is configured by Apache base docker image - no need to add it in your Apache config

Useful links:

-   [Service deployment configuration - Kustomize](https://confluence-colab.hargreaveslansdown.co.uk/display/DG/Service+deployment+configuration+-+Kustomize)